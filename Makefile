SHELL := /bin/bash

download:
	if [[ ! -e "train_dataset/cs.txt.gz" && ! -e "train_dataset/wikipedia.cs" ]]; then\
		mkdir -p train_dataset/;\
		wget https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-2735/cs.txt.gz\
			--directory-prefix=train_dataset/;\
	fi
	if [ ! -e "train_dataset/wikipedia.cs" ]; then\
		cd train_dataset/ && gzip -dc cs.txt.gz > wikipedia.cs;\
	fi

	if [[ ! -e "train_dataset/uk.txt.gz" && ! -e "train_dataset/wikipedia.uk" ]]; then\
		mkdir -p train_dataset/;\
		wget https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-2735/uk.txt.gz\
			--directory-prefix=train_dataset/;\
	fi
	if [ ! -e "train_dataset/wikipedia.uk" ]; then\
		cd train_dataset/ && gzip -dc uk.txt.gz > wikipedia.uk;\
	fi

split:
	mkdir -p dataset_parts_cs/
	mkdir -p dataset_parts_uk/
	# let's split the file into files containing 5 % of the total lines
	# wc -l wikipedia.cs    > 5481438
	cd dataset_parts_cs/ && split -l 100000 ../train_dataset/wikipedia.cs segment
	cd dataset_parts_uk/ && split -l 100000 ../train_dataset/wikipedia.uk segment


prepare_for_training:
	mkdir -p models/

	python3 -m venv venv/
	source venv/bin/activate && pip install wheel
	source venv/bin/activate && pip install -r requirements.txt

	make download
	make split

###############################
############ CZECH ############
###############################
build_czech_unigram_model:
	mkdir -p unigram_model_parts_cs/
	# train model
	source venv/bin/activate && python3 trainngrammodel.py\
		--cpu_count=8\
		--dataset=dataset_parts_cs/\
		--modelparts=unigram_model_parts_cs/\
		--ngrams=1\
		--task=train\
		--verbose

	# merge model
	source venv/bin/activate && python3 trainngrammodel.py\
		--modelparts=unigram_model_parts_cs/\
		--outfile=models/model-1-gram-cs.pickle\
		--task=merge_model_parts\
		--verbose

build_czech_bigram_model:
	mkdir -p bigram_model_parts_cs/
	# train model
	source venv/bin/activate && python3 trainngrammodel.py\
		--cpu_count=8\
		--dataset=dataset_parts_cs/\
		--modelparts=bigram_model_parts_cs/\
		--ngrams=2\
		--task=train\
		--verbose

	# merge model
	source venv/bin/activate && python3 trainngrammodel.py\
		--modelparts=bigram_model_parts_cs/\
		--outfile=models/model-2-gram-cs.pickle\
		--task=merge_model_parts\
		--verbose

###############################
########## UKRAINIAN ##########
###############################
build_ukrainian_unigram_model:
	mkdir -p unigram_model_parts_uk/
	# train model
	source venv/bin/activate && python3 trainngrammodel.py\
		--cpu_count=8\
		--dataset=dataset_parts_uk/\
		--modelparts=unigram_model_parts_uk/\
		--ngrams=1\
		--task=train\
		--language=uk\
		--verbose

	# merge model
	source venv/bin/activate && python3 trainngrammodel.py\
		--modelparts=unigram_model_parts_uk/\
		--outfile=models/model-1-gram-uk.pickle\
		--task=merge_model_parts\
		--verbose

build_ukrainian_bigram_model:
	mkdir -p bigram_model_parts_uk/
	# train model
	source venv/bin/activate && python3 trainngrammodel.py\
		--cpu_count=8\
		--dataset=dataset_parts_uk/\
		--modelparts=bigram_model_parts_uk/\
		--ngrams=2\
		--task=train\
		--language=uk\
		--verbose

	# merge model
	source venv/bin/activate && python3 trainngrammodel.py\
		--modelparts=bigram_model_parts_uk/\
		--outfile=models/model-2-gram-uk.pickle\
		--task=merge_model_parts\
		--verbose

postprocess_czech_unigram_model:
	source venv/bin/activate && source venv/bin/activate && python3 postprocessing.py\
		--modelpath=models/model-1-gram-cs.pickle\
		--outmodelpath=models/model-1-gram-cs-processed\
		--type=unigram

postprocess_czech_bigram_model:
	source venv/bin/activate && source venv/bin/activate && python3 postprocessing.py\
		--modelpath=models/model-2-gram-cs.pickle\
		--outmodelpath=models/model-2-gram-cs-processed\
		--type=bigram

postprocess_ukrainian_unigram_model:
	source venv/bin/activate && source venv/bin/activate && python3 postprocessing.py\
		--modelpath=models/model-1-gram-uk.pickle\
		--outmodelpath=models/model-1-gram-uk-processed\
		--type=unigram

postprocess_ukrainian_bigram_model:
	source venv/bin/activate && python3 postprocessing.py\
		--modelpath=models/model-2-gram-uk.pickle\
		--outmodelpath=models/model-2-gram-uk-processed\
		--type=bigram

postprocess_all:
	make postprocess_czech_unigram_model
	make postprocess_czech_bigram_model

	make postprocess_ukrainian_unigram_model
	make postprocess_ukrainian_bigram_model

build_all:
	make build_czech_unigram_model
	make build_czech_bigram_model

	make build_ukrainian_unigram_model
	make build_ukrainian_bigram_model

build_and_postprocess:
	make build_all
	make postprocess_all

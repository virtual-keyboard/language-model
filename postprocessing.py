import argparse
import json
import os
import pickle
from typing import List

parser = argparse.ArgumentParser()

parser.add_argument(
    '--modelpath',
    required=True,
    type=str,
    help='Path to model to make postprocessing on.'
)
parser.add_argument(
    '--outmodelpath',
    required=True,
    type=str,
    help='Path with filename to save postprocessed model to. Do not include suffix.'
)
parser.add_argument(
    '--type',
    default='unigram',
    type=str,
    help='Which type of model to postprocess. Either `unigram` or `bigram` are valid values.'
)

min_length_to_shorten_unigram = 7
min_length_to_shorten_bigram = 7


def get_similar(words: List[str]):
    """
    Splits list of words into 2D list containing equivalence classes of words
    in terms of common prefix.
    Consider `words` = ['nejznámějšího', 'nejvýznamnější', 'nejvíce', 'nejvýznamnějšího',
        'nejvýznamnějších', 'nejznámější'].
    List of lists representing equivalence class is returned, i.e. [['nejznámějšího', 'nejznámější'],
        ['nejvýznamnější', 'nejvýznamnějšího', 'nejvýznamnějších'], ['nejvíce']]
    :param words: list of words
    :return: equivalence classes of the words
    """
    words.sort()

    eq_classes = []
    processed = 0
    while len(words) != processed:
        eq_class_representative = words[processed]
        tolerance = len(eq_class_representative) - 2

        eq_class = [eq_class_representative]

        processed += 1
        while processed < len(words):
            maybe_member = words[processed]
            # `maybe_member` is in same equivalence class
            # as `eq_class_representative`
            if eq_class_representative[:tolerance] == maybe_member[:tolerance]:
                eq_class.append(maybe_member)
            else:
                break

            processed += 1

        eq_classes.append(eq_class)

    return eq_classes


def get_prefix_of_eq_class(eq_class: List[str]):
    """
    Longest common prefix.
    """
    return os.path.commonprefix(eq_class)


def enhance_model(model, eq_classes_to_consider):
    """
    Longer words with same prefix will be merged into one word in
    a form `longest common prefix`- (dash)
    :param model: model to be enhanced
    :param eq_classes_to_consider: equivalence classes of words.
    :return: enhanced model
    """
    for eq_class in eq_classes_to_consider:
        if len(eq_class) == 1:
            continue

        longest_common_prefix = get_prefix_of_eq_class(eq_class)
        new_total = 0

        for member in eq_class:
            if len(member) < len(longest_common_prefix) - 2:
                continue

            new_total += model[member]
            del model[member]

        model[longest_common_prefix + '-'] = new_total

    return model


def compress_model(model, min_occurrence: int):
    """
    Remove keys of `model` having less than `min_occurrence` occurrences in the corpus.
    """
    compressed_model = {}
    for k, v in model.items():
        if v < min_occurrence:
            continue

        compressed_model[k] = v

    return compressed_model


def save_models(model, args: argparse.Namespace):
    """
    Serialize the model using pickle and JSON.
    :param model: model (in dictionary form)
    :param args: user's arguments
    :return: None
    """
    with open(args.outmodelpath + '.pickle', mode='wb') as f:
        pickle.dump(model, f)

    with open(args.outmodelpath + '.json', mode='w') as f:
        f.write(json.dumps(model, separators=(',', ':')))


def postprocess_unigram_model(args: argparse.Namespace):
    with open(args.modelpath, mode='rb') as f:
        model = pickle.load(f)

    compressed_model = compress_model(model, 20)
    # save_models(compressed_model, args)

    longer_words = list(filter(lambda w: len(w) >= min_length_to_shorten_unigram, list(compressed_model.keys())))
    eq_classes = get_similar(longer_words)

    enhanced_model = enhance_model(compressed_model, eq_classes)

    save_models(enhanced_model, args)


def postprocess_bigram_model(args: argparse.Namespace):
    with open(args.modelpath, mode='rb') as f:
        model = pickle.load(f)

    compressed_model = compress_model(model, 10)
    trie_model = {}

    # transform the dictionary from `(first_word, second_word): count` to
    # `first word: {second word: count}` form to save space.
    for k, v in compressed_model.items():
        first, second = k

        if first not in trie_model:
            trie_model[first] = {}

        trie_model[first][second] = v

    # enhance the words that will be predicted, i.e., the $n$-th words in the $n$-grams.
    for key in trie_model.keys():
        suggestions = list(trie_model[key].keys())
        longer_words = list(filter(lambda w: len(w) >= min_length_to_shorten_bigram, suggestions))
        eq_classes = get_similar(longer_words)
        trie_model[key] = enhance_model(trie_model[key], eq_classes)

    save_models(trie_model, args)


def main(args: argparse.Namespace):
    if args.type == 'unigram':
        postprocess_unigram_model(args)
    elif args.type == 'bigram':
        postprocess_bigram_model(args)


if __name__ == '__main__':
    args = parser.parse_args([] if "__file__" not in globals() else None)
    main(args)

# Language model
This repository focuses on training and serializing the language model for the virtual keyboard's next-word predictions.


## Creating models

```bash
git clone https://gitlab.com/virtual-keyboard/language-model.git
cd language-model/

# creates virtual environment, downloads, and prepares data
make prepare_for_training

# builds the model
make build_and_postprocess
```

#!/usr/bin/env python3
import argparse
import json
import os
import pickle
import random
import re
import time
from collections import defaultdict, deque
import multiprocessing as mp

# tokenizers
from nltk.tokenize import word_tokenize
from tokenize_uk import tokenize_words

parser = argparse.ArgumentParser()
parser.add_argument(
    '--dataset',
    default='dataset_parts/',
    type=str,
    help='Path to text files which will be used to train on.'
)

parser.add_argument(
    '--ngrams',
    default=2,
    type=int,
    help='Parameter $n$ from the word $n$-gram, i.e., we are predicting based on history of length $n-1$.'
)

parser.add_argument(
    '--outfile',
    default='model.pickle',
    type=str,
    help='Model file name.'
)

parser.add_argument(
    '--modelparts',
    default='bigram_model_parts/',
    type=str,
    help='Path to directory where to store the parts of the model.'
)

parser.add_argument(
    '--verbose',
    action='store_true',
    help='Whether to print more information.'
)

parser.add_argument(
    '--task',
    default='train',
    type=str,
    help='Task to run. Either \'train\' or \'merge_model_parts\'.'
)

parser.add_argument(
    '--cutoff_high',
    default=5000000,
    type=int,
    help='Model will not contain words with more than --cutoff_high occurrences.'
)

parser.add_argument(
    '--cutoff_low',
    default=2,
    type=int,
    help='Model will not contain words with less than --cutoff_low occurrences.'
)

parser.add_argument(
    '--cpu_count',
    default=mp.cpu_count(),
    type=int,
    help='Number of CPUs to use for training.'
)

parser.add_argument(
    '--language',
    default='cs',
    type=str,
    help='Language, either \"cs\" or \"uk\".'
)

with open('prepositions.json', mode='rb') as f:
    prepositions = json.load(f)
    prepositions = prepositions.keys()


def word_length(word: str) -> int:
    accented = list('ěéščřžýáíďťňóůú' + 'ěéščřžýáíďťňóůú'.upper())
    return len(word) + sum([c in accented for c in list(word)])


def train(filename: str, args: argparse.Namespace, s: mp.Semaphore):
    """
    Train (`args.ngrams`)-model on file `filename` and
    save it as `args.modelparts`/`filename`.pickle using pickle.
    :param filename: filename to train on.
    :param args: arguments from the user.
    :param s: semaphore to release.
    :return: None
    """
    local_model = defaultdict(lambda: 0)

    if args.verbose:
        print(f'STARTING WITH TRAINING ON {filename}...')

    with open(f'{args.dataset}/{filename}', mode='r') as f:
        train_data = f.read()

    mod = 10000
    ctr = 0

    if args.language == 'cs':
        tokens = word_tokenize(train_data.lower(), language='czech')
    else:
        tokens = tokenize_words(train_data.lower())

    for start_pos in range(len(tokens) - args.ngrams):
        cur_part = tokens[start_pos : start_pos + args.ngrams]
        ctr += 1

        # we do not want to predict words with less than 5 characters
        if word_length(cur_part[-1]) < 5:
            continue

        # we do not want to predict prepositions (there are some longer ones)
        if cur_part[-1] in prepositions:
            continue

        if not all(re.match(r'^[a-zA-ZÀ-ž]+$' if args.language == 'cs' else '^[а-щьюяґєії]+$', w) for w in cur_part):
            continue

        # if `args.ngrams` == 1, then don't make a "1-tuple -> int" mapping, but "str -> int" mapping
        local_model[tuple(cur_part) if args.ngrams > 1 else cur_part[0]] += 1

        if ctr % mod == 0 and args.verbose:
            print(f'Info from {filename}: {ctr}/{len(tokens)}')

    with open(f'{args.modelparts}/{filename}.pickle', mode='wb') as f:
        pickle.dump(dict(local_model), f)

    s.release()


def train_and_save_parts(args: argparse.Namespace):
    """
    Run the training in parallel on files in `args.dataset` directory,
    using `args.cpu_count` CPUs.
    :param args: arguments from the user.
    :return: None
    """
    task_queue = deque()
    for filename in os.listdir(args.dataset):
        task_queue.append(filename)

    s = mp.Semaphore(value=args.cpu_count)

    all_processes = []
    for cur_task in task_queue:
        s.acquire()
        new_process = mp.Process(target=train, args=(cur_task, args, s))
        new_process.start()
        all_processes.append(new_process)

    for process in all_processes:
        process.join()


def merge_model_parts(args: argparse.Namespace):
    """
    Merge all model parts created by training on multiple CPUs and save it
    as `args.outfile`.
    :param args: arguments from the user.
    :return: None
    """
    final_model = {}
    for filename in os.listdir(args.modelparts)[:50]:
        with open(f'{args.modelparts}/{filename}', mode='rb') as f:
            model = pickle.load(f)
            for k, v in model.items():
                if k not in final_model:
                    final_model[k] = v
                else:
                    final_model[k] += v

    with open(args.outfile, mode='wb') as f:
        pickle.dump(final_model, f)


def main(args: argparse.Namespace):
    start = time.time()

    if args.task == 'train':
        train_and_save_parts(args)
    elif args.task == 'merge_model_parts':
        merge_model_parts(args)

    end = time.time()
    print(f'Task took: {end - start} s.')


if __name__ == '__main__':
    args = parser.parse_args([] if "__file__" not in globals() else None)
    main(args)
